(TeX-add-style-hook "sb-macros"
 (function
  (lambda ()
    (LaTeX-add-index-entries
     "check!niveau"
     "check!niveau!A"
     "check!niveau"
     "check!niveau!B"
     "check!niveau"
     "check!niveau!C"
     "check!GM"
     "#1")
    (TeX-add-symbols
     '("rubrique" 1)
     '("motcle" 1)
     '("sujet" 1)
     '("sbcvs" 1)
     '("gnalist" 2)
     '("pagesujet" 1)
     '("titre" 1)
     "niveauA"
     "niveauB"
     "niveauC"
     "GeOCaml"
     "nbretudiants"
     "DrGeOCaml"
     "GeorgesMariano"))))

