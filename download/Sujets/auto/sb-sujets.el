(TeX-add-style-hook "sb-sujets"
 (function
  (lambda ()
    (TeX-run-style-hooks
     "makeidx"
     "sb-macros"
     "hevea"
     "babel"
     "frenchb"
     "estas"
     "times"
     "lastpage"
     "latex2e"
     "art12"
     "article"
     "a4paper"
     "draft"
     "12pt"
     "twoside"))))

